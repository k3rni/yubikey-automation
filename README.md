# Yubikey automation

This is a set of config files that enables some useful behaviors with your (previously configured) YubiKey 4.

On plugging in:
* attempts to load all PKCS11 (PIV) keys into your ssh-agent. This usually requires a PIN, so a dialog for that will pop out.
* if loading was successful, shows a notification. Tries to show how many new keys were loaded.

On unplugging:
* removes these keys, showing a notification
* locks your session

# Requirements

Packages that are already required for using SSH with YubiKey: `openssh-client`, `pcscd` and `opensc-pkcs11` (on Arch: `openssh`, `pcsclite`, `opensc` respectively). Additionally, this uses `seahorse` for the password prompt program. A standard desktop Linux should have all of that already present, maybe except `opensc`.

# Installing

1. Copy files in `systemd/user` to `$HOME/.config/systemd/user`.
2. Edit `yubikey-autoload-keys@.service` so that PKCS points to the proper location of `opensc-pkcs11.so`, and `SSH_ASKPASS` to that of seahorse's `ssh-askpass`.
3. Copy files in `udev/rules.d` to `/etc/udev/rules.d` (a system location, so this requires root)
4. `sudo udevadm control -R`
5. `systemctl --user enable yubikey-plug@.service` (*without* sudo)

This will enable all behaviors.

## Automatically finding required files

Before step 5, run the included `detect-dependencies` script. It will discover these requirements by asking your package manager, and if that fails, searching in some known locations. If successful, it will generate a configuration file, which is then loaded automatically by systemd.

## Using with other Yubikeys or USB tokens

This is YubiKey-specific only because it reacts to only one device being plugged in. In theory, it should work with any CCID readers and USB tokens that `pcscd` recognizes.

To change what device this reacts to, change `PRODUCT` and `INTERFACE` in the udev rules file. Look up the proper values with `udevadm info -q all sysfs-path-to-your-device`. To get the sys path, run `udevadm monitor` and look for lines that look like:

```
KERNEL[16555.617870] add      /devices/pci0000:00/0000:00:01.3/0000:02:00.0/usb1/1-9/1-9:1.2
```
The sysfs path always starts with `/sys/bus/usb/devices` and follows with the bus address, which is the last part of the above string. So for this device, the full sysfs path is `/sys/bus/usb/devices/1-9:1.2`. Once you have that, run `udevadm info`:

```
P: /devices/pci0000:00/0000:00:01.3/0000:02:00.0/usb1/1-9/1-9:1.2
L: 0
E: DEVPATH=/devices/pci0000:00/0000:00:01.3/0000:02:00.0/usb1/1-9/1-9:1.2
E: DEVTYPE=usb_interface
E: DRIVER=usbhid
E: PRODUCT=1050/407/435
E: TYPE=0/0/0
E: INTERFACE=11/0/0
E: MODALIAS=usb:v1050p0407d0435dc00dsc00dp00ic03isc00ip00in01
E: SUBSYSTEM=usb
[trim irrelevant lines]
```

Use the full `PRODUCT` string, or any prefix of it followed by an asterisk. To match devices by different vendors, just duplicate the rules for each new device. The `INTERFACE` string isn't as important, but it must match only one of the exposed interfaces (YK4 has three, two of them being USBHID for keyboard emulation). The default setting matches the CCID interface (type `0b` or `11` decimal), which there is only one. If multiple devices are detected, the service will start multiple instances and attempt to import keys more than once, which may be an error.

## Customizing

Edit `yubikey-plug@.service`, and comment out or remove `Wants=` lines for the two sub-services. Run `systemctl --user daemon-reload` afterwards. Test with unplugging and re-plugging your device.

### Notification language

Notifications are always displayed in English. Using translations from systemd units is possible, but cumbersome. To display them in your language, just edit the autoload service file and change the message strings.
