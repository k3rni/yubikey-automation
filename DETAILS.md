# The rules file (a quick primer to udev rules)

```
ACTION=="add", SUBSYSTEM=="usb", ENV{INTERFACE}=="11/0/0", ENV{PRODUCT}=="1050/407/*", TAG+="systemd", ENV{SYSTEMD_USER_WANTS}+="yubikey-plug@%k.service" 
ACTION=="remove", SUBSYSTEM=="usb", ENV{INTERFACE}=="11/0/0", ENV{PRODUCT}=="1050/407/*", TAG+="systemd", ENV{SYSTEMD_USER_WANTS}+="yubikey-plug@%k.service" 
```

These are rules and directives. Rules match existing data, directives set things in udev. Matching is done with double-equals `==`, and setting with a single equal, or a `+=` in case of lists. Separate rules for add and remove are required, even if they look identical otherwise, this is because of how systemd-udev interaction currently works. After making changes in rules files, reload udev database with `udevadm control -R`.

* `ACTION`: rather self-explanatory, match on when device is added and when it is removed
* `SUBSYSTEM`: we only care about USB devices. Some other buses also support hotplug (e.g. SATA, PCIE) but we don't want events from them.
* `ENV{PRODUCT}`: matches a vendor/product/version string. We need to use this and not `ATTR{idVendor}`, see [systemd#7587](https://github.com/systemd/systemd/issues/7587#issuecomment-381428545)
* `ENV{INTERFACE}`: a USB device may export multiple interfaces, and the Yubikey does just that with three, two of them being USBHID keyboards for OTP typing. We need to match a single interface, doesn't really matter which one - this one is the CCID interface
* `TAG`: convention to tag all rules that invoke systemd in some capacity with this. Udev will not invoke systemd otherwise.
* `ENV{SYSTEMD_USER_WANTS}`: most important directive: tell systemd we want to couple a named service to that device. 

Additionally, the service name uses a `@%k` path. This is to invoke a new service name for each distinct plug-in location (port), and therefore a) allow multiple keys to work, and b) to actually start/stop the service with plugging/unplugging.

# Systemd service unit

.config/systemd/user/yubikey-plug@.service (Commented)
```ini
[Unit]
Description=Yubikey Hotplug
# Stop when device unit that started this service (de-facto the udev rule) goes inactive
StopWhenUnneeded=true
# Launch these services if possible, but don't fail if they do
Wants=yubikey-autoload-keys@%i.service
Wants=yubikey-lock-screen@%i.service

[Service]
# This is a service that runs a command, but doesn't remain in the background
Type=oneshot
# And once that initial command exits, it wants to still be considered active.
RemainAfterExit=yes
# Don't do any extra process cleanup when stopping the service. It's considered done after the ExecStop command exits
KillMode=none
# Start service by running this command.
# %i is replaced with whatever was after the `@` sign in systemd wants line. which will be the usb port specification.
# This service also doesn't do anything except being an anchor point for the other two, set up in Wants= lines above.
ExecStart=/usr/bin/logger Yubikey Start %i
ExecStop=/usr/bin/logger Yubikey Stop %i

[Install]
# Systemd needs to know where to install this service, this is a typical destination for graphical desktops.
WantedBy=multi-user.target
```

The `%i` string is replaced with the device's usb location, typically something like `1-9:1.2`. That indicates bus 1, which contains ports directly on/connected to your motherboard, port 9 (depends on enumeration order, but all ports have a unique number), device 1, interface 2.
If on a USB hub, the path is longer: something like `1-5.4:1.2`, indicating in order: bus 1, port 5, subport 4 (on the external hub), device 1, interface 2. Ultimately, all that matters is that it's unique for every different location the key can be plugged into.

References:

* http://www.reactivated.net/writing_udev_rules.html
* https://unix.stackexchange.com/questions/63232/what-is-the-correct-way-to-write-a-udev-rule-to-stop-a-service-under-systemd
* https://superuser.com/questions/1364509/how-can-i-start-a-systemd-service-when-a-given-usb-device-ethernet-dongle-is-p (most complete answer)
* https://www.freedesktop.org/software/systemd/man/systemd.service.html
* https://www.freedesktop.org/software/systemd/man/systemd.unit.html
* https://www.freedesktop.org/software/systemd/man/systemd.device.html

# Debugging udev+systemd

* `journalctl -f` to watch systemd logs.
* `udevadm monitor` to watch udev logs
* `udevadm test -a ACTION /sys/bus/usb/devices/1-9:1.2`, where ACTION is add or remove, and path is the sysfs path to your device. To find yours, run `lsusb -t` and trace the tree to your device; or watch `udevadm monitor` when you plug in the key.
* `udevadm info -q all syspath` to query device info
* `udevadm control -R` to reload udev rules after editing
* `systemctl --user daemon-reload` to reload systemd units after editing

